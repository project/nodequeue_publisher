<?php


/**
 * Implements hook_form_FORM_ID_alter()
 */
function nodequeue_publisher_form_nodequeue_edit_queue_form_alter(&$form, $form_state) {

  $qid = !empty($form['qid']['#value']) ? $form['qid']['#value'] : NULL;

  // Add our submit handler
  $form['#submit'][] = 'nodequeue_publisher_form_nodequeue_edit_queue_form_alter_submit';

  // Add the form elements
  $form['nodequeue_publisher'] = array(
    '#type' => 'fieldset',
    '#title' => t('Nodequeue publisher'),
    '#description' => t('Using these options you can specify a schedule for publishing nodes from this nodequeue.'),
    '#collapsed' => FALSE,
    '#collapsible' => TRUE,
    // Make sure its above the submit buttons.
    '#weight' => 0.005,
    '#tree' => TRUE,
  );

  $settings = nodequeue_publisher_get_queue_settings($qid);

  $form['nodequeue_publisher']['enable'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable scheduled publishing'),
    '#default_value' => $settings['enable'],
  );

  $form['nodequeue_publisher']['enabled_days'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Publish only on the selected days.'),
    '#options' => array(
      t('Sunday'),
      t('Monday'),
      t('Tuesday'),
      t('Wednesday'),
      t('Thursday'),
      t('Friday'),
      t('Saturday'),
    ),
    '#default_value' => $settings['enabled_days'],
  );

  $form['nodequeue_publisher']['frequency'] = array(
    '#type' => 'textfield',
    '#title' => t('Publication frequency.'),
    '#default_value' => $settings['frequency'],
    '#description' => t('Enter a number that will be combined with the option below.')
  );

  $form['nodequeue_publisher']['frequency_interval'] = array(
    '#type' => 'select',
    '#title' => t('Publication frequency interval.'),
    '#default_value' => $settings['frequency_interval'],
    '#options' => array(
      strtotime('+1 minute', 0) => t('Minute'),
      strtotime('+1 hour', 0) => t('Hour'),
      strtotime('+1 day', 0) => t('Day'),
      strtotime('+1 week', 0) => t('Week'),
    ),
  );

  $form['nodequeue_publisher']['update_created'] = array(
    '#type' => 'checkbox',
    '#title' => t('Update created time'),
    '#default_value' => $settings['update_created'],
  );

}

/**
 * Form submission callback.
 *
 * @see nodequeue_publisher_form_nodequeue_edit_queue_form_alter().
 */
function nodequeue_publisher_form_nodequeue_edit_queue_form_alter_submit($form, &$form_state) {
  $settings = $form_state['values']['nodequeue_publisher'];
  $settings['enabled_days'] = array_filter($settings['enabled_days']);
  $qid = $form_state['values']['qid'];
  if (!empty($qid)) {
    nodequeue_publisher_set_queue_settings($qid, $settings);
  }
}

/**
 * Get the publisher settings for the queue.
 */
function nodequeue_publisher_get_queue_settings($qid = NULL) {
  $settings = variable_get('nodequeue_publisher_queue_settings_' . $qid, array()) +
    _nodequeue_publisher_settings_defaults();
  return $settings;
}

/**
 * Get the default settings for nodequeue_publisher.
 */
function _nodequeue_publisher_settings_defaults() {
  return array(
    'enable' => FALSE,
    'enabled_days' => range(0, 6),
    'frequency' => 1,
    'frequency_interval' => strtotime('+1 day', 0),
    'update_created' => FALSE,
  );
}

/**
 * Set the nodequeue settings.
 */
function nodequeue_publisher_set_queue_settings($qid, $settings) {
  // Add the defaults.
  $settings += _nodequeue_publisher_settings_defaults();
  variable_set('nodequeue_publisher_queue_settings_' . $qid, $settings);
}

/**
 * Implements hook_cron().
 */
function nodequeue_publisher_cron() {
  // Loop over all queues.
  // TODO: nodequeue doesn't allow us to get all queues, so we get 1000 instead.
  $queues = nodequeue_get_all_qids(1000);
  foreach ($queues as $qid => $queue) {
    $publisher_settings = nodequeue_publisher_get_queue_settings($qid);
    if ($publisher_settings['enable']) {
      // We need to act.
      nodequeue_publisher_act($qid, $publisher_settings);
    }
  }
}

/**
 * Acts on a particular queue with given settings.
 */
function nodequeue_publisher_act($qid, $settings) {
  // Should we be acting today.
  $today = date('N', REQUEST_TIME) % 7;
  if (!in_array($today, $settings['enabled_days'])) {
    drush_print_r('not acting today');
    return;
  }

  // Have we acted already.
  $last_acted = variable_get('nodequeue_publisher_queue_act_time_' . $qid, 0);
  $freq = $settings['frequency'] * $settings['frequency_interval'];
  if ((REQUEST_TIME - $last_acted) < $freq) {
    drush_print_r('not acting now');
    return;
  }

  // Free to act.

  $subqueue = reset(nodequeue_load_subqueues_by_queue($qid));

  // Get a node if we can.
  if ($node = array_shift(nodequeue_load_nodes($subqueue->sqid, FALSE, 0, 1, FALSE))) {
    $node->status = NODE_PUBLISHED;
    if ($settings['update_created']) {
      $node->created = REQUEST_TIME;
    }
    node_save($node);
    nodequeue_subqueue_remove_node($subqueue->sqid, $node->nid);
  }

  // Record that we ran.
  variable_set('nodequeue_publisher_queue_act_time_' . $qid, REQUEST_TIME);
}
